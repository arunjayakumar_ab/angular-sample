import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { ProductdetailsComponent } from './components/productdetails/productdetails.component';

import {DatasharingService} from './services/products/datasharing.service';
import {CookieService} from 'angular2-cookie/services/cookies.service';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProductlistComponent,
    PagenotfoundComponent,
    ProductdetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    DatasharingService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
