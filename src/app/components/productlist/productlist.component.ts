import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {config} from '../../config';
import {Router,ActivatedRoute} from '@angular/router';
import {DatasharingService} from '../../services/products/datasharing.service';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css'],
})
export class ProductlistComponent implements OnInit {

  constructor(
    private http:HttpClient,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private selectedProductData :DatasharingService,
    private cookie:CookieService
  ) { }

  ngOnInit() {
    this.getProducts();
  }



  title:string  = "Products";
  products = [];
  config = config;
  token:String = this.cookie.get('token');
  // token:String = localStorage.getItem('token');

  getProducts(){
    let URL = config.api + 'product/fetchProduct';
    let data = 'token=' + this.token + '&&' +
                'uniqueId=' + '5964ccf3cd5940520e0726fe' + '&&' +
                'page='  + 1 + '&&' +
                'limit=' + 1000
                  ;
    let header = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    let headers = { headers : header};

    this.http.post(URL,data,headers)
      .subscribe((result)=>{
          console.log(result);
          this.products = result['result'];
      });

  }

  showProductDetails(product){
    if(!product){
      return;
    }
    this.sendProductDetails(product);
    this.router.navigate([ product._id],{relativeTo:this.activatedRoute});

  }

  sendProductDetails(product){
    this.selectedProductData.setProductDetails(product);
  }

}
