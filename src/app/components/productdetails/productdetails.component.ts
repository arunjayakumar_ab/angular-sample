import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DatasharingService} from '../../services/products/datasharing.service';
import {config} from '../../config';


@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private data: DatasharingService) {

  }

  config = config;
  sub;
  id: String;
  productDetails;


  ngOnInit() {
    this.log();
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log("id", this.id);
      this.productDetails = this.data.getProductDetails();
      console.log("productDetails", this.productDetails);
      if (!this.productDetails || !Object.keys(this.productDetails).length) {
        this.goToListingPage();
      }
    });
  }


  log() {
    console.log("on product detail");
  }

  goToListingPage() {
    this.router.navigate(['list']);
  }
  closePopup(){
    this.goToListingPage();
  }

}
