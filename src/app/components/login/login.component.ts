import { Component, OnInit } from '@angular/core';
import Login from './login';
import {validateEmaill} from '../../utils/stringUtils';
import {config} from '../../config';
import {Router} from '@angular/router';

import {HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
// import {Http} from '@angular/http'
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login:Login = {
    email : "",
    password : ""

    };

  constructor(
    private http:HttpClient,
    private router:Router,
    private cookie:CookieService
  ) { }

  ngOnInit() {
  }

  userLogin(){
    console.log(this.login);
    if(!validateEmaill(this.login.email)){
      alert("invalid email");
      return;
    }

    if(!this.login.password){
      alert("invalid password");
      return;
    }

    let URL = config.api + 'user/login';
    let data = 'email=' + this.login.email + '&&' +
                'password=' + this.login.password
                ;
    let header = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    let headers = { headers : header};
    this.http.post(URL,data,headers)
      .subscribe((result)=>{
          console.log("result",result);
          if(result['success']){
            localStorage.setItem('token',result['result']['token']);
            this.cookie.put('token',result['result']['token']);
            this.router.navigate(['/list'])
          }
      });



  }

}
