import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductlistComponent} from './components/productlist/productlist.component';
import {LoginComponent} from './components/login/login.component';
import {PagenotfoundComponent} from './components/pagenotfound/pagenotfound.component';
import {ProductdetailsComponent} from './components/productdetails/productdetails.component';

const routes: Routes = [
  {path : '',redirectTo : 'login' ,pathMatch: 'full'},
  {
    path : "list",
    component : ProductlistComponent,
    children : [
      {path :":id",component : ProductdetailsComponent},
      // {path : "**",redirectTo : '/' }
    ]
  },
  {path : "login",component : LoginComponent },
  {path : "**",component : PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
