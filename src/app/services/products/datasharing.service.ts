import { Injectable } from '@angular/core';

@Injectable()
export class DatasharingService {

  constructor() { }

  productDetails = {};

  setProductDetails(details){
    this.productDetails = details;
  }

  getProductDetails(){
    return this.productDetails;
  }

}
